const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

let mailConfig;
if (process.env.NODE_ENV === "production") {
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_SECRET,
        },
    };
    mailConfig = sgTransport(options);
} else {
    if (process.env.NODE_ENV === "staging") {
		console.log('XXXXXXXXXXXXX');
        const options = {
            auth: {
                api_key: process.env.SENDGRID_API_SECRET,
            },
        };
        mailConfig = sgTransport(options);
    } else {
        mailConfig = {
            host: 'smtp.ethereal.email',
    		port: 587,
    		auth: {
       				user: process.env.ethereal_user,
        			pass: process.env.ethereal_pwd
   			 },
        };
    }
}




module.exports = nodemailer.createTransport(mailConfig);
/*
//Generar el servicio SMTP con la cuenta de ethereal.email

nodemailer.createTestAccount((err, account)=>{

	if (err) {

		console.error('Fallo la creacion del est en la cuenta' + err.message);

		return process.exit(1);

	}

	// Si no tenemos error, seguimos para obtener las credenciales...

	console.log('Credenciales obtenidas...');

	// Crear datos de la conexion...

	let transporter = nodemailer.createTransport({

		host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: 'clifton.maggio@ethereal.email',
        pass: '9q1mj7Y1twEDK2nuyU'
    }

	});

		// Creamos el mensaje (objeto)

	let message = {

			from: 'cmulero83@icloud.com',

			to: 'grayce.wisoky91@ethereal.email',

			subject: 'Nodemailr is unicode friendly',

			text: 'Hola mundo...',

			html: '<p><b>Hola</b> mundo...</p>'

	};

	// Realizamos el envio...

	transporter.sendMail(message, (err, info)=>{

		if (err) {

			console.log('Ocurrio un error', + err.message);

			return process.exit(1);

		}

		console.log('Mensaje enviado:  %s ', info.messageId);

		// Solo si se enviar a traves de Ethereal acount

		console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

	});

});

*/